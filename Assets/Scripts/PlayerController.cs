﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public static PlayerController instance;

    public enum Direction {Left, Right};

    public Bullet bulletPrefab;
    public Text shieldText;

    private Transform t;
    private ParticleSystem deathParticle;
    private GameObject cube;
    private GameObject shield;

    private int[] lanes = new int[] { -2, -1, 0, 1, 2 };
    private int currentLane = 2;

    private Vector3 startPos;

    private bool isJumping;
    private float jumpVelocity;
    private float shieldTime;

    private Vector3 pos;

    private Collider col;

    private bool shieldActivated;
    public bool dead;

	// Use this for initialization
	void Start ()
    {
        DOTween.Init();

        instance = this;

        t = transform;

        startPos = t.position;
        pos = startPos;

        col = GetComponent<Collider>();
        deathParticle = t.Find("DeathParticle").GetComponent<ParticleSystem>();
        cube = t.Find("Cube").gameObject;
        shield = t.Find("Shield").gameObject;

        //startGame();
    }
	
	// Update is called once per frame
	void Update ()
    {
        if(MainController.isPlaying && !dead)
        {
#if UNITY_EDITOR
            if(Input.GetKeyDown(KeyCode.D))
            {
                moveLane(Direction.Right, 1);
            }
            else if(Input.GetKeyDown(KeyCode.A))
            {
                moveLane(Direction.Left, 1);
            }
            else if(Input.GetKeyDown(KeyCode.Space))
            {
                jump();
            }
            else if(Input.GetKeyDown(KeyCode.Z))
            {
                shoot();
            }
#endif
            if(isJumping)
            {
                pos = t.position;

                jumpVelocity += Physics.gravity.y * Time.deltaTime;
                pos.y += jumpVelocity * Time.deltaTime;

                if(pos.y <= 0)
                {
                    isJumping = false;
                    pos.y = 0;
                }
                t.position = pos;
            }

            if(shieldActivated)
            {
                shieldTime -= Time.deltaTime;

                if(shieldTime <= 0)
                {
                    shieldTime = 0;
                    shieldActivated = false;
                    shield.SetActive(false);
                }

                //shieldText.text = shieldTime.ToString("N1");
            }
            shieldText.text = shieldTime.ToString("N1");
        }
		
    }

    public void moveLane(Direction direction, int numLanes)
    {
        if(direction == Direction.Left)
        {
            currentLane -= numLanes;
            if(currentLane < 0)
            {
                currentLane = 0;
            }
            
            RaycastHit hit;
            if(Physics.BoxCast(col.bounds.center, col.bounds.size / 2, -t.right, out hit, t.rotation, numLanes))
            {
                if(hit.collider.tag == "Hazard")
                {
                    if(hit.collider.GetComponent<Wall>() != null)
                    {
                        float x = hit.collider.gameObject.transform.localPosition.x;
                        currentLane = getLaneFromXPos(x) + 1;
                    }
                }
            }
        }
        else
        {
            currentLane += numLanes;
            if(currentLane >= lanes.Length)
            {
                currentLane = lanes.Length - 1;
            }
           
            RaycastHit hit;
            if(Physics.BoxCast(col.bounds.center, col.bounds.size / 2, t.right, out hit, t.rotation, numLanes))
            {
                if(hit.collider.tag == "Hazard")
                {
                    if(hit.collider.GetComponent<Wall>() != null)
                    {
                        float x = hit.collider.gameObject.transform.localPosition.x;
                        currentLane = getLaneFromXPos(x) - 1;
                    }
                }
            }
        }
        
        t.DOMoveX(lanes[currentLane], 0.25f);
    }
    public void jump()
    {
        isJumping = true;
        jumpVelocity = 6;
    }
    public void activateShield()
    {
        shieldActivated = true;
        shieldTime = 5;

        shield.SetActive(true);
    }
    public void shoot()
    {
        Vector3 bulletPos = t.position;
        bulletPos += t.forward;
        bulletPos.y = 0.5f;

        Bullet bullet = Instantiate(bulletPrefab, bulletPos, Quaternion.identity);
    }
    public void reset()
    {
        transform.position = new Vector3(0, 0, -10);
        shield.SetActive(false);
        cube.SetActive(true);
        shieldTime = 0;
        dead = false;
    }
    public void startGame()
    {
        transform.DOMoveZ(-3f, 2f);
    }
    private int getLaneFromXPos(float x)
    {
        switch(Mathf.RoundToInt(x))
        {
            case -2:
                return 0;
            case -1:
                return 1;
            case -0:
                return 2;
            case 1:
                return 3;
            case 2:
                return 4;
            default:
                return 0;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log(other.gameObject);
        if(other.tag == "Hazard")
        {
            die();
        }
        else if(other.tag == "LaserHazard")
        {
            if(!shieldActivated)
            {
                die();
            }
        }
    }
    private void die()
    {
        dead = true;

        cube.SetActive(false);
        shield.SetActive(false);

        deathParticle.Emit(50);
    }
}
