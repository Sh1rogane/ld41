﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallLaser : MonoBehaviour
{
    private Transform laser;
    private ParticleSystem laserParticle;

	// Use this for initialization
	void Start ()
    {
        laser = transform.Find("Laser");
    }
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        RaycastHit hit;
        if(Physics.Raycast(laser.position, -transform.right, out hit))
        {
            //Debug.DrawLine(laser.position, hit.point, Color.green);
            laser.localScale = new Vector3(hit.distance, 1, 1);
        }
        else
        {
            //Debug.DrawRay(laser.position, -transform.right * 10, Color.red);
        }
	}
}
