﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "AttackCard", menuName = "Cards/AttackCard")]
public class AttackCard : Card
{
    public override void play()
    {
        PlayerController.instance.shoot();
    }
}
