﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ShieldCard", menuName = "Cards/ShieldCard")]
public class ShieldCard : Card
{
    public override void play()
    {
        PlayerController.instance.activateShield();
    }
}
