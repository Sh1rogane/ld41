﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "JumpCard", menuName = "Cards/JumpCard")]
public class JumpCard : Card
{
    public override void play()
    {
        PlayerController.instance.jump();
    }
}
