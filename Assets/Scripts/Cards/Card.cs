﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Card : ScriptableObject
{
    public string cardName;
    public Sprite cardArt;

    public virtual void play()
    {
        
    }
}
