﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "MoveCard", menuName = "Cards/MoveCard")]
public class MoveCard : Card
{
    public PlayerController.Direction direction;

    public int numLanes = 1;

    public override void play()
    {
        PlayerController.instance.moveLane(direction, numLanes);
    }
}
