﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{

	// Use this for initialization
	void Start ()
    {
        Destroy(gameObject, 3f);
    }
	
	// Update is called once per frame
	void Update ()
    {
        transform.Translate(0, 0, 5 * Time.deltaTime);
	}

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Hazard")
        {
            Wall wall = other.GetComponent<Wall>();
            if(wall != null)
            {
                wall.die();
            }
            Destroy(gameObject);
        }
    }
}
