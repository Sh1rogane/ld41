﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wall : MonoBehaviour
{
    public GameObject cube;
    public ParticleSystem deathParticle;

    private Collider col;
	// Use this for initialization
	void Start () {
        col = GetComponent<Collider>();

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void die()
    {
        StartCoroutine(_die());
    }
    private IEnumerator _die()
    {
        col.enabled = false;
        cube.SetActive(false);

        deathParticle.Emit(50);

        yield return new WaitForSeconds(0.5f);

        Destroy(gameObject);
    }
}
