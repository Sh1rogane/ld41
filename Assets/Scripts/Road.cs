﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Road : MonoBehaviour
{
    private Transform hazardsTransform;

    private bool[,] spots = new bool[5, 20]; 

	// Use this for initialization
	void Start ()
    {
        hazardsTransform = transform.Find("Hazards");
    }
	
	// Update is called once per frame
	void Update ()
    {
		
	}
    public void addHazard(GameObject hazard, int colum, int row)
    {
        GameObject go = Instantiate(hazard, hazardsTransform, false);
        go.transform.localPosition = getPositionFromColumRow(colum, row);
        setUsedSpot(colum, row);
    }
    public void addWallHazard(GameObject hazard, bool rightWall)
    {
        GameObject go = Instantiate(hazard, hazardsTransform, false);
        if(rightWall)
        {
            go.transform.localPosition = new Vector3(2.5f, 0, 0);
            go.transform.localRotation = Quaternion.Euler(0, 0, 0);
        }
        else
        {
            go.transform.localPosition = new Vector3(-2.5f, 0, 0);
            go.transform.localRotation = Quaternion.Euler(0, 180, 0);
        }
    }
    public void clearHazards()
    {
        foreach(Transform child in hazardsTransform)
        {
            Destroy(child.gameObject);
        }
        spots = new bool[5, 20];
    }
    public Vector2 getRandomValidPoint()
    {
        int colum = Random.Range(0, 5);
        int row = Random.Range(0, 20);

        while(!validSpot(colum, row))
        {
            colum = Random.Range(0, 5);
            row = Random.Range(0, 20);

        }

        return new Vector2(colum, row);
    }
    public Vector2 getRandomValidPointRowDistance(int minRowDistance)
    {
        Vector2 v = getRandomValidPoint();

        int count = 0;

        while(nearestUsedRow((int)v.y) <= minRowDistance)
        {
            v = getRandomValidPoint();

            count++;
            if(count > 100)
            {
                break;
            }
                
        }

        return v;
    }

    private int nearestUsedRow(int row)
    {
        int nearest = int.MaxValue;

        for(int x = 0; x < 5; x++)
        {
            for(int y = 0; y < 20; y++)
            {
                if(y == row)
                {
                    continue;
                }
                if(spots[x,y])
                {
                    int dist = Mathf.Abs(row - y);
                    if(dist < nearest)
                    {
                        nearest = dist;
                    }
                }
            }
        }
        return nearest;
    }

    public bool validSpot(int colum, int row)
    {
        return !spots[colum, row];
    }
    private void setUsedSpot(int colum, int row)
    {
        spots[colum, row] = true;
    }
    private Vector3 getPositionFromColumRow(int colum, int row)
    {
        return new Vector3(colum - 2, 0, row - 9);
    }
}
