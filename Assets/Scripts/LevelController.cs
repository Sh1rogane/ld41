﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelController : MonoBehaviour
{
    public enum Hazards { Wall, Spikes, Mines, WallLaser, LaserShot}

    public Text scoreText;
    private float score;

    public List<Road> roads = new List<Road>();

    public GameObject wallHazardPrefab;
    public GameObject spikeHazardPrefab;
    public LaserShot laserShotHazard;
    public GameObject wallLaserHazardPrefab;

    public float speed = 3;

    private float laserShotTimer;
    private float laserShotTime = 15;

	// Use this for initialization
	IEnumerator Start ()
    {
        yield return new WaitForSeconds(0.1f);
        //generateHazards(roads[1]);
        //generateHazards(roads[2]);
    }
	
	// Update is called once per frame
	void Update ()
    {
        if(MainController.isPlaying && !PlayerController.instance.dead)
        {
            for(int i = 0; i < roads.Count; i++)
            {
                roads[i].transform.Translate(0, 0, -speed * Time.deltaTime, Space.Self);
            }

            if(roads[0].transform.localPosition.z <= -20)
            {
                Vector3 newPos = roads[2].transform.localPosition;
                newPos.z += 20;
                roads[0].transform.localPosition = newPos;

                roads[0].clearHazards();

                Road temp = roads[0];
                roads[0] = roads[1];
                roads[1] = roads[2];
                roads[2] = temp;

                generateHazards(roads[2]);
            }

            laserShotTimer += Time.deltaTime;
            if(laserShotTimer > laserShotTime)
            {
                laserShotTimer = 0;
                laserShotHazard.setColum(Random.Range(0, 5));
                laserShotHazard.fire();
            }

            score += Time.deltaTime * 10;

            scoreText.text = "" + Mathf.FloorToInt(score);
        }
    }
    public void startGame()
    {
        generateHazards(roads[1]);
        generateHazards(roads[2]);
    }
    public void reset()
    {
        laserShotTimer = 0;

        for(int i = 0; i < roads.Count; i++)
        {
            roads[i].clearHazards();
            roads[i].transform.localPosition = new Vector3(0, 0, i * 20);
        }

        score = 0;
        scoreText.text = "" + Mathf.FloorToInt(score);
    }
    private void generateHazards(Road road)
    {
        generateWalls(road);

        generateFloorHazards(road);

        generateWallHazards(road);
    }

    private void generateWalls(Road road)
    {
        for(int i = 0; i < 3; i++)
        {
            Vector2 point = road.getRandomValidPointRowDistance(2);
            road.addHazard(wallHazardPrefab, (int)point.x, (int)point.y);
        }
    }
    private void generateFloorHazards(Road road)
    {
        float rand = Random.value;

        if(rand > 0.1f)
        {
            Vector2 point = road.getRandomValidPointRowDistance(2);
            road.addHazard(spikeHazardPrefab, (int)point.x, (int)point.y);

            if(rand > 0.5f)
            {
                point = road.getRandomValidPointRowDistance(2);
                road.addHazard(spikeHazardPrefab, (int)point.x, (int)point.y);
            }
        }
    }
    private void generateWallHazards(Road road)
    {
        float rand = Random.value;
        if(rand >= 0.75f)
        {
            float dirR = Random.value;
            if(dirR > 0.5)
            {
                road.addWallHazard(wallLaserHazardPrefab, true);
            }
            else
            {
                road.addWallHazard(wallLaserHazardPrefab, false);
            }

            //Vector2 point = road.getRandomValidPointRowDistance(2);
            //road.addHazard(wallLaserHazardPrefab, (int)point.x, (int)point.y);
        }
    }
}
