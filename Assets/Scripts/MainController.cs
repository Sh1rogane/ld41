﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class MainController : MonoBehaviour
{
    [Header("Main menu")]
    public RectTransform mainMenu;

    [Header("Game over")]
    public RectTransform gameOver;
    public Text scoreText;

    [Header("How to play")]
    public RectTransform howToPlay;

    [Header("Controllers")]
    public LevelController levelController;
    public CardController cardController;
    public PlayerController playerController;

    private bool displayGameOver;

    public static bool isPlaying;

	// Use this for initialization
	void Start ()
    {
        AspectUtility.SetCamera();
	}
	
	// Update is called once per frame
	void Update ()
    {
		if(!displayGameOver && playerController.dead)
        {
            displayGameOver = true;
            showGameOver();
        }

        if(Input.GetKeyDown(KeyCode.S))
        {
            //ScreenCapture.CaptureScreenshot("Screenshot_" + System.DateTime.Now.ToFileTime() + ".png");
        }
	}

    private void showGameOver()
    {
        scoreText.text = levelController.scoreText.text;

        gameOver.DOAnchorPosY(0, 1f).SetDelay(1f);
    }

    public void onPlay()
    {
        mainMenu.DOAnchorPosY(-1280, 1f);
        reset();
    }
    public void onHowToPlay()
    {
        howToPlay.DOAnchorPosY(0, 1f);
    }
    public void onTryAgain()
    {
        reset();
    }
    public void onBack()
    {
        howToPlay.DOAnchorPosY(-1280, 1f);
    }

    public void reset()
    {
        displayGameOver = false;
        levelController.reset();
        cardController.reset();
        playerController.reset();

        playerController.startGame();
        levelController.startGame();

        gameOver.DOAnchorPosY(-1280, 1f);

        isPlaying = true;
    }
}
