﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class CardSlot : MonoBehaviour
{
    private Image cardTimer;
    private CardImage cardImage;

    private float cardTimerFill;

    private Card card;

    private float oldMoveY;
    private bool isDown;

    private float downTime;

	// Use this for initialization
	void Start ()
    {
        cardTimer = transform.Find("CardTimer").GetComponent<Image>();
        cardImage = transform.Find("Card").GetComponent<CardImage>();

        cardImage.setAlpha(0);
    }
	
	// Update is called once per frame
	void Update ()
    {
        if(MainController.isPlaying && !PlayerController.instance.dead)
        {
            if(card == null)
            {
                cardTimerFill += Time.deltaTime / 2;
                cardTimer.fillAmount = cardTimerFill;

                if(cardTimerFill >= 1)
                {
                    getNewCard();
                }
            }

            if(Input.GetMouseButtonDown(0) && isInside(cardImage.rt, Input.mousePosition) && card != null)
            {
                oldMoveY = normalizedMousePosition().y;
                isDown = true;

                downTime = Time.time;
            }
            if(isDown && Input.GetMouseButtonUp(0))
            {
                isDown = false;
                if(cardImage.rt.anchoredPosition.y > 100 || Time.time - downTime < 0.15f)
                {
                    useCard();
                }
                else
                {
                    cardImage.rt.DOAnchorPosY(0, 0.5f);
                    cardImage.fadeAlpha(1, 0.25f);
                }
            }

            if(isDown)
            {
                float deltaMove = normalizedMousePosition().y - oldMoveY;
                oldMoveY = normalizedMousePosition().y;

                float y = Mathf.Clamp(cardImage.rt.anchoredPosition.y + (deltaMove * 1280), -100, 250);

                cardImage.setPosition(0, y);
                cardImage.setAlpha(1 - (y * 0.0025f));
            }
        }
	}

    private void getNewCard()
    {
        cardTimerFill = 0;
        cardTimer.fillAmount = cardTimerFill;
        card = CardController.instance.getNewCard();

        cardImage.setAlpha(1);

        cardImage.setPosition(0, -350);
        cardImage.setCard(card);

        cardImage.rt.DOAnchorPosY(0, 0.5f);
    }
    public void reset()
    {
        card = null;
        cardImage.fadeAlpha(0, 0f);
        cardTimerFill = 0;
        cardTimer.fillAmount = cardTimerFill;
    }
    public void useCard()
    {
        if(card != null)
        {
            cardTimerFill = 0;

            card.play();

            card = null;

            cardImage.rt.DOAnchorPosY(350, 0.5f);
            cardImage.fadeAlpha(0, 0.25f);
        }
    }
    private Vector2 normalizedMousePosition()
    {
        Vector2 nMousePos = Input.mousePosition;
        nMousePos.x /= Screen.width;
        nMousePos.y /= Screen.height;

        return nMousePos;
    }
    private bool isInside(RectTransform rt, Vector2 pos)
    {
        return RectTransformUtility.RectangleContainsScreenPoint(rt, pos, CardController.instance.cam);
    }
}
