﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class CardImage : MonoBehaviour
{
    public RectTransform rt;

    private Image cardImage;
    private Text cardText;
    private CanvasGroup canvasGroup;

	// Use this for initialization
	void Awake ()
    {
        rt = transform as RectTransform;
        cardImage = GetComponent<Image>();
        cardText = rt.Find("Text").GetComponent<Text>();
        canvasGroup = GetComponent<CanvasGroup>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void setPosition(float x, float y)
    {
        rt.anchoredPosition = new Vector2(x, y);
    }
    public void setCard(Card card)
    {
        cardImage.sprite = card.cardArt;
        cardText.text = card.cardName;
    }
    public void setAlpha(float alpha)
    {
        canvasGroup.alpha = alpha;
    }

    public void fadeAlpha(float alpha, float time)
    {
        canvasGroup.DOFade(alpha, time);
    }
}
