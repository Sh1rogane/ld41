﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class LaserShot : MonoBehaviour
{
    private SpriteRenderer warningSprite;
    private GameObject laser;
	// Use this for initialization
	void Start ()
    {
        warningSprite = transform.Find("Warning").GetComponent<SpriteRenderer>();
        laser = transform.Find("Laser").gameObject;

        warningSprite.DOFade(0, 0);
        laser.SetActive(false);

        //InvokeRepeating("fire", 10, 10);
    }
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public void fire()
    {
        StartCoroutine(_fire());
    }

    private IEnumerator _fire()
    {
        warningSprite.DOFade(1, 1);

        yield return new WaitForSeconds(2f);

        warningSprite.DOFade(0, 0.25f);

        laser.SetActive(true);
        laser.transform.DOScaleX(0.7f, 0.25f);
        laser.transform.DOScaleY(2f, 0.25f);

        yield return new WaitForSeconds(0.5f);

        laser.transform.DOScaleX(0, 1f);
        laser.transform.DOScaleY(0, 1f);

        yield return new WaitForSeconds(1f);

        laser.SetActive(false);
    }

    public void setColum(int colum)
    {
        Vector3 newPos = transform.position;
        newPos.x = colum - 2f;
        transform.position = newPos;
    }
}
