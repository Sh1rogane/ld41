﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class CardController : MonoBehaviour
{
    [Header("References")]
    public List<CardSlot> cardSlots = new List<CardSlot>();

    [Header("Cards")]
    public List<Card> moveCardList = new List<Card>();
    public List<Card> actionCardList = new List<Card>();

    public static CardController instance;

    public Camera cam;
	// Use this for initialization
	void Awake ()
    {
        instance = this;
        cam = Camera.main;
    }
	
	// Update is called once per frame
	void Update ()
    {
		if(Input.GetKeyDown(KeyCode.Alpha1))
        {
            cardSlots[0].useCard();
        }
        else if(Input.GetKeyDown(KeyCode.Alpha2))
        {
            cardSlots[1].useCard();
        }
        else if(Input.GetKeyDown(KeyCode.Alpha3))
        {
            cardSlots[2].useCard();
        }
        else if(Input.GetKeyDown(KeyCode.Alpha4))
        {
            cardSlots[3].useCard();
        }
    }

    public Card getNewCard()
    {
        float rand = Random.value;
        if(rand >= 0.5f)
        {
            int randCard = Random.Range(0, moveCardList.Count);
            return Instantiate(moveCardList[randCard]);
        }
        else
        {
            int randCard = Random.Range(0, actionCardList.Count);
            return Instantiate(actionCardList[randCard]);
        }
        
    }

    public void reset()
    {
        for(int i = 0; i < cardSlots.Count; i++)
        {
            cardSlots[i].reset();
        }
    }
}
